// app.js
const express = require('express');
const bodyParser = require('body-parser');

const user = require('./routes/user.route');
const postroute = require('./routes/post.route');
const food = require('./routes/food.route');

const app = express();

// Set up mongoose connection
const mongoose = require('mongoose');
//Change this to be running on the aws server instead of mlab
//  Private Ip only works on the same network public works on any network should be private to be more secure
let dev_db_url = 'mongodb://172.31.5.61:27017/foodapp';
// let dev_db_url = 'mongodb://34.244.247.23:27017/foodapp';
let mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// Accepted Formats
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: false},{limit: '50mb'}));
// Routes
app.use('/user', user);
app.use('/post', postroute);
app.use('/food',food);

let port = 3000;

app.listen(port, () => {
    console.log('Server is up and running on port number ' + port);
});
