const User = require('../models/user.model');
const Token = require('../models/user.confirmation');
var crypto = require('crypto');
var nodemailer = require('nodemailer');
const fs = require("fs");
const path = require("path");
var bcrypt = require('bcryptjs');


exports.user_create = function (req, res) {
    var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(req.body.password, salt);
    let user = new User(
        {
            name: req.body.name,
            email: req.body.email,
            password: hash,
            username: req.body.username,
            signupdate: req.body.signupdate
        }
    );

    user.save(function (err, user) {
        if (err) {
            return res.send("Mongo Error " + err);
        }
        console.log("User saved");

        // var userid = user.id;
        // const token = new Token({_userId: userid, token: crypto.randomBytes(16).toString('hex')});
        // // Save the verification token
        // token.save(function (err) {
        //     if (err) {
        //         return res.status(500).send({msg: err.message});
        //     }
        //
        //     // Send the email
        //     let transporter = nodemailer.createTransport({
        //         host: 'smtp.gmail.com',
        //         port: 465,
        //         secure: true,
        //         auth: {
        //             type: 'OAuth2',
        //             user: 'user@example.com',
        //             accessToken: 'ya29.Xx_XX0xxxxx-xX0X0XxXXxXxXXXxX0x'
        //         }
        //     });
        //     const mailOptions = {
        //         from: 'michealdunne14@gmail.com', // sender address
        //         to: 'michealdunne14@gmail.com', // list of receivers
        //         subject: 'Hello ✔', // Subject line
        //         text: 'Hello world ?', // plaintext body
        //     };
        //
        //     transporter.sendMail(mailOptions, function (error, info) {
        //         if (error) {
        //             return console.log(error);
        //         }
        //         console.log('Message sent: ' + info.response);
        //     });
        //
        //     // transporter.sendMail(mailOptions, function (err) {
        //     //     if (err) { return res.status(500).send({ msg: err.message }); }
        //     //     res.status(200).send('A verification email has been sent to ' + user.email + '.');
        //     // });
        // });
        res.send(user)
    })
};

//Get following Data
exports.following_data = function (req,res) {
    let array = [];
    let followingArray = [];

    User.findById(req.body.id, function (err, user) {
        if (err) return res.send(err);

        if (user !== null) {
            if (user.following.length === 0) {
                res.send("No Following Data")
            } else {
                user.following.forEach(function(followingUser, index, array) {
                    User.findById(followingUser.followingoid, function (err, userFollowing) {
                        if (userFollowing !== null) {
                            console.log("Get images following");
                            get_images_following(res, userFollowing, followingArray, user)
                        }
                    });
                });
            }
        }else{
            res.send("No Following Data")
        }
    });
};

//Get user details
exports.user_details = function (req, res) {
    User.findById(req.params.id, function (err, user) {
        if (err) return res.send(err);

        console.log("Get images");
        get_images(res, user);
    })
};

//Get user details by email
exports.user_details_email = function (req,res) {
    var query = User.where({ 'email': req.body.email});
    try {
        query.findOne(function (err, user) {
            if (err) {
                return res.send("No User Found");
            }

            if (user === null) {
                console.log("User email or password is incorrect");
                res.send("User email or password is incorrect")
            } else if (bcrypt.compareSync(req.body.password, user.password)) {
                console.log("Sending back user...");
                get_images(res, user);
            } else {
                console.log("User email or password is incorrect");
                res.send("User email or password is incorrect")
            }
        })
    }catch (e) {
        res.send("Mongo Error")
    }
};

const get_images_multisearch = function(res,users) {
    try {
        console.log("Get multiple images...");
        let userArrayList = [];
        users.forEach(function (user) {
            try {
                console.log("Read directory");
                fs.readdir(user.profilePicture, (err, files) => {
                    files.forEach(file => {
                        let absolutePath = path.join(user.profilePicture, file);
                        fs.readFile(absolutePath, function read(err, data) {
                            if (err) {
                                throw err;
                            }

                            console.log("Read File");

                            let base64Image = Buffer.from(data).toString('base64');
                            userArrayList.push({image: base64Image, user: user});
                            if (userArrayList.length === users.length) {
                                res.send({'userArray': userArrayList});
                                console.log("user array");
                            }
                        });
                    });
                });
            }catch (e) {
                console.log(e)
            }
        });
    }catch (e) {
        console.log(e)
    }
};

//get images
const get_images = function(res,user) {
    try {
        console.log("Read directory");
        fs.readdir(user.profilePicture, (err, files) => {
            files.forEach(file => {
                let absolutePath = path.join(user.profilePicture, file);
                fs.readFile(absolutePath, function read(err, data) {
                    if (err) {
                        throw err;
                    }

                    console.log("Read File");

                    let base64Image = Buffer.from(data).toString('base64');
                    res.send({
                        image: base64Image,
                        user: user
                    });
                });
            });
        });

    }catch (e) {
        console.log(e)
    }
};

const get_images_following = function(res,userFollowing,userArray,user) {
    try {
        console.log("Read Directory");
        fs.readdir(userFollowing.profilePicture, (err, files) => {
            files.forEach(file => {
                let absolutePath = path.join(userFollowing.profilePicture, file);
                fs.readFile(absolutePath, function read(err, data) {
                    if (err) {
                        throw err;
                    }

                    console.log("Read File");

                    let base64Image = Buffer.from(data).toString('base64');
                    userArray.push({image: base64Image,user: userFollowing});
                    if (userArray.length === user.following.length){
                        res.send({'userArray': userArray})
                    }
                });
            });
        });

    }catch (e) {
        console.log(e)
    }
};


//Get user details username
exports.user_details_username = function (req,res) {
    let regex = new RegExp(req.body.username,'i');

    User.find({
        $or: [
            {'username': regex}
        ]
    }).exec(function(err,users){
        console.log("Get images search by username");
        get_images_multisearch(res, users);
    });
};

//User updated
exports.user_update = function (req, res) {
    User.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, user) {
        if (err) return console.log(err);

        console.log("User updated");
        res.send('User updated.');
    });
};

let postToFolder = "";
const multer = require('multer');

const multerController = multer({dest: "uploads"}).single('profilePic');
const handleError = (err, res) => {
    res
        .status(500)
        .contentType("text/plain")
        .end("Oops! Something went wrong!");
};

module.exports.uploadFiles = async (req, res) => {
    try {
        postToFolder = req.params.id;
        let dir = __dirname + "/uploads/profilePicture" + postToFolder;

        User.findById(postToFolder, function (err, user) {
            if (err){
                return console.log(err);
            }

            user.profilePicture = dir;
            console.log("User get by profile image");

            user.save(function (err) {
                if (err) {
                    return console.log(err);
                }

                console.log("User save");
                multerController(req, res, err => {
                    postingImages(req.file,res,dir);
                    return res.send(user);
                })
            });
        });
    } catch (error) {
        if (error.code === "LIMIT_UNEXPECTED_FILE") {
            return res.send("Too many files to upload.");
        }
        return res.send(`Error when trying upload many files: ${error}`);
    }
};

//This does not upload it correctly
function postingImages(value,res,dir){
    const fileName = value.originalname;

    const tempPath = value.path;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    const targetPath = path.join(dir, fileName);

    fs.rename(tempPath, targetPath, err => {
        if (err) return handleError(err, res);
    });
}

//Follow and be followed by that user
exports.user_follow = function(req,res){
    User.findById(req.params.id, function (err, user) {
        if (err) return console.log(err);
        user.following.forEach(function(value){
            if (value.followingoid === req.body.followingoid) {
                console.log('User already Followed');
            }
        });
        console.log('User Followed');
        user.following.push({followingoid: req.body.followingoid});

        User.findById(req.body.followingoid, function (err, followedUser) {
            if (err) return console.log(err);

            console.log('User Following');
            followedUser.followers.push({followeroid: req.params.id});
            followedUser.save(function (err) {
                if (err) {
                    return console.log(err);
                }
            });
        });

        user.save(function (err) {
            if (err) {
                return console.log(err);
            }

            console.log('Following User and user is followed');
            res.send('Following User and user is followed')
        });
    });
};

//unfollowed user
exports.user_unfollow = function(req,res){
    User.findById(req.params.id, function (err, user) {
        if (err) return console.log(err);

        user.following.forEach(function(value, index, array){
            if (value.followingoid === req.body.followingoid) {
                user.following.splice(index,1);
                console.log('Removed User following');
            }

        });

        User.findById(req.body.followingoid, function (err, followedUser) {
            if (err) return console.log(err);

            followedUser.followers.forEach(function(value, index, array){
                if (value.followeroid === req.params.id){
                    followedUser.followers.splice(index,1);
                    console.log('followedUser removed');

                }
            });

            followedUser.save(function (err) {
                if (err) {
                    return console.log(err);
                }
                console.log('followedUser saved');
            });
        });

        user.save(function (err) {
            if (err) {
                return console.log(err);
            }
            res.send("User Updated")
        });
    });
};

//Add food item to cupboard
exports.add_food_to_cupboard = function(req,res){
    let cupboardData = [];
    let incrementCounter = 0;
    let foodCounter = 0;
    User.findById(req.params.id, function (err, user) {
        if (err) return console.log(err);

        console.log('user adding to cupboard');
        if (req.body.cupboard.length > 0) {
            req.body.cupboard.forEach(function(postCupboard, cupboardIndex, cupboardArray) {
                if (user.cupboard.length > 0) {
                    foodCounter = user.cupboard.length;
                    let foundFood = false;
                    console.log('User add information to cupboard');
                    user.cupboard.forEach(function (userCupboard, index, array) {
                        if (postCupboard.cupboardoid === userCupboard.cupboardoid) {
                            incrementCounter++;
                            foundFood = true;
                            console.log('Add on items that are already in basket');
                            incrementFoodCupboard(user,req,res,postCupboard,index,userCupboard,cupboardIndex,array,cupboardArray)
                        }
                    });
                    if (!foundFood) {
                        console.log('Adding data to the cupboard 1');
                        addDataCupboard(user,req,res,postCupboard,cupboardData,incrementCounter,foodCounter)
                    }
                }else {
                    console.log('Adding data to the cupboard 2');
                    addDataCupboard(user,req,res,postCupboard,cupboardData,incrementCounter,foodCounter)
                }
            });

        }else{
            res.send('No cupboard')
        }
    });
};

//Add information to the basket
const addDataBasket = function (user,req,res,postBasket,basketData,incrementCounter,foodCounter) {
    //Push data to basket
    basketData.push({basketoid: postBasket.basketoid, counter: postBasket.counter});
    if (req.body.basket.length === basketData.length + incrementCounter) {
        console.log('Add data to basket');
        basketData.forEach(function (userCupboard){
            user.basket.push(userCupboard);
            if (user.basket.length === basketData.length + foodCounter) {
                user.save(function (err) {
                    if (err) {
                        return console.log(err);
                    }

                    console.log('Added data to basket');
                    res.send('Added data to basket')
                });
            }
        });
    }
};

//Add data to the cupboard
const addDataCupboard = function (user,req,res,postCupboard,cupboardData,incrementCounter,foodCounter) {
    console.log('Entered add to cupboard');
    cupboardData.push({cupboardoid: postCupboard.cupboardoid, foodPurchasedCounter: postCupboard.foodPurchasedCounter});
    if (req.body.cupboard.length === cupboardData.length + incrementCounter) {
        cupboardData.forEach(function (userCupboard){
            user.cupboard.push(userCupboard);
            if (user.cupboard.length === cupboardData.length + foodCounter) {
                user.save(function (err) {
                    if (err) {
                        return console.log(err);
                    }
                    console.log('Added data to cupboard');
                    res.send('Added data to cupboard')
                });
            }
        });
    }
};

const incrementFoodCupboard = function(user,req,res,postCupboard,index,userCupboard,cupboardIndex,array,cupboardArray) {
    user.cupboard[index].foodPurchasedCounter += postCupboard.foodPurchasedCounter;
    if (cupboardIndex === cupboardArray.length - 1) {
        user.save(function (err) {
            if (err) {
                return console.log(err);
            }

            console.log('Increment food cupboard');
            res.send('Added data to cupboard')
        });
    }
};


const incrementFoodBasket = function(user,req,res,postCupboard,index,userBasket,basketIndex,array,basketArray) {
    user.basket[index].counter += postCupboard.counter;
    if (basketIndex === basketArray.length - 1) {
        user.save(function (err) {
            if (err) {
                return console.log(err);
            }

            console.log('Increment food basket');
            res.send('Added data to basket')
        });
    }
};

//Add food to basket
exports.add_food_to_basket = function(req,res){
    let basketData = [];
    let incrementCounter = 0;
    let foodCounter = 0;
    User.findById(req.params.id, function (err, user) {
        if (err) return console.log(err);

        if (req.body.basket.length > 0) {
            console.log('Basket length is greater than 0');
            req.body.basket.forEach(function(postCupboard, basketIndex, basketArray) {
                if (user.basket.length > 0) {
                    foodCounter = user.basket.length;
                    let foundFood = false;
                    user.basket.forEach(function (userBasket, index, array) {
                        if (postCupboard.basketoid === userBasket.basketoid) {
                            incrementCounter++;
                            foundFood = true;
                            console.log('Add on to the food basket');
                            incrementFoodBasket(user,req,res,postCupboard,index,userBasket,basketIndex,array,basketArray)
                        }
                    });
                    if (!foundFood) {
                        console.log('Add data to basket 1');
                        addDataBasket(user,req,res,postCupboard,basketData,incrementCounter,foodCounter)
                    }
                }else {
                    console.log('Add data to basket 2');
                    addDataBasket(user,req,res,postCupboard,basketData,incrementCounter,foodCounter)
                }
            });

        }else{
            res.send('No basket')
        }
    });
};

//Add saved post to profile
exports.add_saved_post = function(req,res){
    User.findById(req.params.id, function (err, user) {
        if (err) return console.log(err);

        if (req.body.savedoid !== "") {
            console.log("Save post");
            user.saved.push({savedoid: req.body.savedoid});

            user.save(function (err) {
                if (err) {
                    return console.log(err);
                }

                console.log("User saved a post");
                res.send('User saved a post')
            });
        }else{
            res.send('Fill in fields')
        }
    });

};

//Delete a user
exports.user_delete = function (req, res) {
    User.findByIdAndRemove(req.params.id, function (err) {
        if (err) return console.log(err);
        res.send('Deleted successfully!');
    })
};

//Remove item from basket
exports.remove_from_basket = function (req, res) {
    User.findById(req.body.id, function (err, user) {
        if (user !== null) {
            for (let i = 0; i < user.basket.length; i++) {
                if (user.basket[i].basketoid === req.body.basketoid){
                    user.basket.splice(i, 1);
                    console.log("Item removed from basket");
                }
            }
            user.save(function (err) {
                if (err) {
                    return console.log(err);
                }
                res.send("removed item from basket")
            });
        }
    });
};

//Remove item from cupboard
exports.remove_from_cupboard = function (req, res) {
    User.findById(req.body.id, function (err, user) {
        if (user !== null) {
            for (let i = 0; i < user.cupboard.length; i++) {
                if (user.cupboard[i].cupboardoid === req.body.cupboardoid){
                    user.cupboard.splice(i, 1);
                    console.log("Item removed from cupboard");
                }
            }
            user.save(function (err) {
                if (err) {
                    return console.log(err);
                }
                res.send("removed item from cupboard")
            });
        }
    });
};

//Change item counter for basket
exports.change_counter_basket = function (req, res) {
    User.findById(req.body.id, function (err, user) {
        if (user !== null) {
            for (let i = 0; i < user.basket.length; i++) {
                if (user.basket[i].basketoid === req.body.basketoid){
                    user.basket[i].counter = req.body.counter;
                    console.log("Change counter on basket");
                }
            }
            user.save(function (err) {
                if (err) {
                    return console.log(err);
                }
                res.send("updated Counter")
            });
        }
    });
};

//Change item counter for cupboard
exports.change_counter_cupboard = function (req, res) {
    User.findById(req.body.id, function (err, user) {
        if (user !== null) {
            for (let i = 0; i < user.cupboard.length; i++) {
                if (user.cupboard[i].cupboardoid === req.body.cupboardoid){
                    user.cupboard[i].foodPurchasedCounter = req.body.foodPurchasedCounter;
                    console.log("Change counter on cupboard");
                }
            }

            user.save(function (err) {
                if (err) {
                    return console.log(err);
                }
                res.send("Updated Counter")
            });
        }
    });
};
