const Food = require('../models/food.model');
const User = require('../models/user.model');
const Post = require('../models/post.model');
const fs = require("fs");
const path = require("path");

exports.add_food = function (req, res) {
    let food = new Food(
        {
            name: req.body.name,
            price: req.body.price,
            shop: req.body.shop,
            expirationTime: req.body.expirationTime
        }
    );

    food.save(function (err,food) {
        if (err) {
            return console.log(err);
        }
        console.log("Save Food");
        res.send(food)
    });
};

let postToFolder = "";
const multer = require('multer');

const multerController = multer({dest: "uploads"}).single('foodPic');
const handleError = (err, res) => {
    res
        .status(500)
        .contentType("text/plain")
        .end("Oops! Something went wrong!");
};

//Add images to File system storage
module.exports.uploadFiles = async (req, res) => {
    try {
        postToFolder = req.params.id;
        let dir = __dirname + "/uploads/food" + postToFolder;

        Food.findById(postToFolder, function (err, food) {
            if (err){
                return console.log(err);
            }

            food.imagePath = dir;

            console.log("Food Upload files");

            food.save(function (err,food) {
                if (err) {
                    return console.log(err);
                }
                console.log("Food Saved");
                multerController(req, res, err => {
                    postingImages(req.file,res,dir);
                    return res.send(food);
                })
            });
        });

    } catch (error) {
        if (error.code === "LIMIT_UNEXPECTED_FILE") {
            return res.send("Too many files to upload.");
        }
        return res.send(`Error when trying upload many files: ${error}`);
    }
};

function postingImages(value,res,dir){
    const fileName = value.originalname;

    const tempPath = value.path;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    const targetPath = path.join(dir, fileName);

    fs.rename(tempPath, targetPath, err => {
        if (err) return handleError(err, res);
    });
}

// Get images of food items
const get_images_food = async function(res,food,foodInitialArray,lengthBodyArray,index) {
    return new Promise((resolve, reject) => {
        if (food != null) {
            console.log("Attempting to get images for food");

            return new Promise((resolve, reject) => {
                return fs.readdir(food.imagePath, (err, files) => {
                    return err !== null ? reject(err) : resolve(files)
                })
            }).then(function (files) {

                console.log("Got Files");

                let promises = [];
                let _foodInitialArray = [];

                for (let i = 0; i < files.length; i++) {
                    const file = files[i];
                    const absolutePath = path.join(food.imagePath, file);

                    console.log("Reading file");

                    promises.push(
                        new Promise((resolve, reject) => {
                            fs.readFile(absolutePath, function (err, data) {
                                console.log("Adding to foodInitialArray");

                                _foodInitialArray.push({image: Buffer.from(data).toString('base64'), food: food});
                                resolve();
                            })
                        })
                    );
                }

                return Promise.all(promises).then(function () {
                    console.log("Done all images retrieval");
                    resolve(_foodInitialArray);
                })
            })
                .catch(function (err) {
                    console.log("Error occured during retrieval attempt");
                    console.log(err);

                    reject(err);
                })
        } else {
            console.log("No food query found");
            resolve();
        }
    })
};

//Get images when listing food
const get_images_foodList = function(food,res,index,array,valueArray) {
    try {
        console.log("Reading Directory");
        fs.readdir(food.imagePath, (err, files) => {
            try {
                files.forEach(file => {
                    let absolutePath = path.join(food.imagePath, file);
                    fs.readFile(absolutePath, function read(err, data) {
                        if (err) {
                            throw err;
                        }
                        console.log("Reading files");

                        valueArray.push({image: Buffer.from(data).toString('base64'), food: food});
                        if (index === array.length - 1) {
                            res.send({'foodArray': valueArray});
                        }
                    });
                });
            }catch (e) {
                console.log(e)
            }
        });

    }catch (e) {
        console.log(e)
    }
};
//Get images of food
const get_images = function(res,food) {
    try {
        if (food !== null) {
            console.log("Reading food Directory");
            fs.readdir(food.imagePath, (err, files) => {
                try {
                    files.forEach(file => {
                        let absolutePath = path.join(food.imagePath, file);
                        fs.readFile(absolutePath, function read(err, data) {
                            if (err) {
                                throw err;
                            }

                            console.log("Reading food file");

                            res.send({
                                image: Buffer.from(data).toString('base64'),
                                food: food
                            });
                        });
                    });
                } catch (e) {
                    console.log(e)
                }
            });
        }else{
            console.log("No Food Found");
            res.send("No Food Found")
        }

    }catch (e) {
        console.log(e)
    }
};

//Find food by the shop
exports.find_food_shop = function (req,res) {
    try {
        if (req.body.shopArray.length > 0) {
            let shopBool = false;
            for(let i = 0;i < req.body.shopArray.length;i++){

            }
            req.body.shopArray.forEach((value, index, array) => {
                var query = Food.where({"shop": value.shop});
                query.findOne(function (err, shop) {
                    if (err) {
                        return console.log("No Shops Found");
                    }

                    if (shop !== null && !shopBool) {
                        res.send(shop);
                        console.log("Found Shop");
                        shopBool = true
                    }else{
                        if (!shopBool && index === array.length -1) {
                            console.log("No Shops Found");
                        }
                    }
                });
            });
        }
    }catch (e) {
        console.log(e)
    }
};

//Find food by name
exports.find_food_name = function (req,res) {
    var query = Food.where({ 'name': req.body.name});
    query.findOne(function (err, food){
        if (err) {
            return res.send("Error");
        }

        console.log("Getting food images");
        get_images(res, food);
    }).catch(function(err) {
        return res.send({error: err})
    })
};

// Find food by post id
exports.find_food_postid = function (req,res) {
    Post.findById(req.body.id, function (err, post) {
        if (err) {
            return console.log(err);
        }
        let valueArray = [];
        console.log("Getting post ingredients");
        post.ingredients.forEach(function (ingredientsId,idx, array) {
            try {
                Food.findById(ingredientsId.ingredientoid, function (err, food) {
                    get_images_foodList(food,res,idx,array,valueArray);
                });
            } catch (e) {
                console.log(e)
            }
        });
    });
};

//Get the food by partial text search
exports.find_food_partialtext = function(req,res){
    let regex = new RegExp(req.body.name,'i');
    Food.find({
        $or: [
            {'name': regex}
        ]
    }).exec(function(err,food){
        let valueArray = [];
        console.log("Getting food item partial text");
        food.forEach(function (food,idx, array) {
            get_images_foodList(food,res,idx,array,valueArray);
        });
    });
};


//Finding the food for the receipt
exports.find_food_name_initial = function (req,res) {
    let foodInitialArray = [];
    try {

        if (req.body.foodArray.length > 0) {

            let promises = [];

            console.log("Starting");

            for (let i = 0; i < req.body.foodArray.length; i++) {
                const value = req.body.foodArray[i];

                console.log(value);
                promises.push(
                    new Promise((resolve, reject) => {
                        return Food.findOne({ $and:
                                [
                                    {'name': value.name},
                                    {'shop': value.shop}
                                ]}).then(function (food) {
                            console.log("Done query for food");

                            return get_images_food(res, food, foodInitialArray, req.body.foodArray.length, i).then(function (_foodInitialArray) {
                                foodInitialArray = foodInitialArray.concat(_foodInitialArray);

                                console.log("Resolving");
                                resolve(foodInitialArray)
                            })
                        })
                        .catch(function () {
                            resolve();
                        })
                    })
                );
            }

            return Promise.all(promises).then(function () {
                // console.log(index);
                console.log("Returned from getting images");
                foodInitialArray = foodInitialArray.filter(function (data) {
                    return data ? data : false;
                });

                if (foodInitialArray.length !== 0) {
                    console.log("food array is not null");
                    res.send({'foodArray': foodInitialArray});
                } else {
                    console.log(foodInitialArray);
                    res.send("no items found")
                }
            })
        }
    } catch(err) {
        console.log(err);
    }
};

//Get items that are in the cupboard
exports.cupboard_id_initial = function (req,res) {
    let foodInitialArray = [];
    let valueArray = [];
    try {
        console.log("Cupboard get by id initial");
        User.findById(req.body.id, function (err, user){
            if (err) {
                return console.log(err);
            }

            if (user !== null) {
                let itemsProcessed = 0;
                if (user.cupboard.length === 0) {
                    res.send("no cupboard data")
                } else {
                    try {
                        user.cupboard.forEach(function (value) {
                            itemsProcessed++;
                            console.log("Items processed" + itemsProcessed);
                            if (value.cupboardoid !== "") {
                                foodInitialArray.push(value.cupboardoid);
                                if (itemsProcessed === user.cupboard.length) {
                                    console.log("foodListData");
                                    // Get images of each food item
                                    foodListData(res, foodInitialArray, valueArray)
                                }
                            }
                        });
                    } catch (e) {
                        console.log(e)
                    }
                }
            }else{
                res.send("no cupboard data")
            }
        })


    }catch (e) {
        console.log(e)
    }
};

// Get items in the basket
exports.basket_id_initial = function (req,res) {
    let foodInitialArray = [];
    let valueArray = [];
    try {
        console.log("Finding items in basket");
        User.findById(req.body.id, function (err, user){
            if (err) {
                return console.log(err);
            }
            let itemsProcessed = 0;
            if (user.basket.length === 0){
                res.send("no basket data")
            }else {
                user.basket.forEach(function (value) {
                    itemsProcessed++;
                    if (value.basketoid !== "") {
                        foodInitialArray.push(value.basketoid);
                        if (itemsProcessed === user.basket.length) {
                            console.log("foodListData");
                            foodListData(res, foodInitialArray, valueArray)
                        }
                    }
                });
            }
        })

    }catch (e) {
        console.log(e)
    }
};

const foodListData = function(res, postsArray,valueArray){
    postsArray.forEach(function (value,idx, array) {
        try {
            Food.findById(value, function (err, food) {
                console.log("Get images in foodListData");
                get_images_foodList(food,res,idx,array,valueArray);
            });
        } catch (e) {
            console.log(e)
        }
    });
};



//Find food by id
exports.find_food_id = function(req,res){
    Food.findById(req.params.id, function (err, food){
        if (err) {
            return console.log(err);
        }

        console.log("Found food by id");
        get_images(res,food);
    })
};

//Put shop reliablity
exports.put_shop_reliability = function (req,res) {
    Food.findByIdAndUpdate(req.params.id, {$inc: {shopReliability: 1}}, function (err, response) {
        if (err) {
            res.send(err);
        } else {
            console.log("Put shop reliability");
            res.send("Food updated");
        }
    });
};

//Remove Shop reliability
exports.remove_shop_reliability = function (req,res) {
    Food.findByIdAndUpdate(req.params.id, {$inc: {shopReliability: -1}}, function (err, response) {
        if (err) {
            res.send(err);
        } else {
            console.log("remove shop reliability");
            res.send("Food updated");
        }
    });
};

//put Price reliability
exports.put_price_reliability = function (req,res) {
    Food.findByIdAndUpdate(req.params.id, {$inc: {priceReliability: 1}}, function (err, response) {
        if (err) {
            res.send(err);
        } else {
            console.log("Put price reliability");
            res.send("Food updated");
        }
    });
};

//Remove price reliability
exports.remove_price_reliability = function (req,res) {
    Food.findByIdAndUpdate(req.params.id, {$inc: {priceReliability: -1}}, function (err, response) {
        if (err) {
            res.send(err);
        } else {
            console.log("Remove shop reliability");
            res.send("Food updated");
        }
    });
};
// put expiration time reliability
exports.put_expirationTime_reliability = function (req,res) {
    Food.findByIdAndUpdate(req.params.id, {$inc: {expirationTimeReliability: 1}}, function (err, response) {
        if (err) {
            res.send(err);
        } else {
            console.log("Put expiration time reliability");
            res.send("Food updated");
        }
    });
};

//Remove Expiration time reliability
exports.remove_expirationTime_reliability = function (req,res) {
    Food.findByIdAndUpdate(req.params.id, {$inc: {expirationTimeReliability: -1}}, function (err, response) {
        if (err) {
            res.send(err);
        } else {

            console.log("Remove expiration time reliability");
            res.send("Food updated");
        }
    });
};

//Remove image path reliability
exports.remove_imagePath_reliability = function (req,res) {
    Food.findByIdAndUpdate(req.params.id, {$inc: {imagePathReliability: -1}}, function (err, response) {
        if (err) {
            res.send(err);
        } else {

            console.log("Remove image reliability");
            res.send("Food updated");
        }
    });
};


//Put image path reliability
exports.put_imagePath_reliability = function (req,res) {
    Food.findByIdAndUpdate(req.params.id, {$inc: {imagePathReliability: 1}}, function (err, response) {
        if (err) {
            res.send(err);
        } else {

            console.log("Put image reliability");
            res.send("Food updated");
        }
    });
};
