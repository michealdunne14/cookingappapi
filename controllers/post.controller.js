const Post = require('../models/post.model');
const userController = require('../controllers/user.controller');
const User = require('../models/user.model');
const upload = require("../middleware/upload");
const fs = require("fs");
const path = require("path");

//Create Post
exports.post_create = function (req, res) {
    let post = new Post(
        {
            title: req.body.title,
            description: req.body.description,
            method: req.body.method,
            ingredients: req.body.ingredients,
            difficulty: req.body.difficulty,
            completionTime: req.body.completionTime,
            useroid: req.params.id,
            posttime: Date.now()
        }
    );

    //Save Post
    post.save(function (err,post) {
        if (err) {
            return console.log(err);
        }
        console.log("Saved Post");
        var postId = post.id;
        User.findById(req.params.id, function (err, user) {
            if (err){
                return console.log(err);
            }
            //Save Post Id to user Posts
            user.posts.push({ postoid: postId, posttime: Date.now(),hearts: 0});
            console.log("Added post to user");


            user.save(function (err) {
                if (err) {
                    return console.log(err);
                }
                console.log("Saved User");
            });
        });
        res.send("PostOid " + post._id)
    })
};

// Retrieve Post details
module.exports.post_details = function (req, res) {
    User.findById(req.body.id, function (err, user) {
        if (err) return res.send(err);

        let valueArray = [];
        let postsArray =  [];
        try {
            if (user !== null) {
                console.log("User is not Null");
                //Checks if the user has no posts or following
                if (user.posts.length === 0 && user.following.length === 0) {
                    res.send("No Posts Found")
                } else {
                    console.log("User has posts or following");
                    //Add all user posts to array
                    user.posts.forEach(function (value) {
                        postsArray.push({postoid: value.postoid, posttime: value.posttime, hearts: value.hearts})
                    });
                    if (user.following.length !== 0) {
                        //Search threw following posts
                        user.following.forEach((followingUser, index, array) => {
                            //Find Following Users
                            User.findById(followingUser.followingoid, function (err, followingUser) {
                                if (followingUser.posts.length !== 0) {
                                    followingUser.posts.forEach((followingPost, postIndex, postArray) => {
                                        // Add Following users posts to the array
                                        postsArray.push({
                                            postoid: followingPost.postoid,
                                            posttime: followingPost.posttime,
                                            hearts: followingPost.hearts
                                        });
                                        console.log("Added post" + followingPost.postoid);
                                        // Checks if on last following user
                                        if (index === array.length - 1 && postIndex === postArray.length -1) {
                                            sortPostData(postsArray, res, valueArray, req);
                                            console.log("Sorting Posts 1");
                                        }
                                    })
                                } else {
                                    console.log("User had no posts");
                                    //If last following user has no posts then go to next function
                                    if (index === array.length - 1) {
                                        sortPostData(postsArray, res, valueArray, req);
                                        console.log("Sorting Posts 2");
                                    }
                                }
                            });
                        })
                    } else {
                        //If user has no following then go to the next function
                        sortPostData(postsArray, res, valueArray, req);
                        console.log("Sorting Posts 3");
                    }
                }
            }else {
                res.send("No Posts Found")
            }
        }catch (e) {
            console.log(e)
        }
    });
};

// Get more post data
module.exports.post_new_data = function (req, res) {
    User.findById(req.body.id, function (err, user) {
        if (err) return res.send(err);

        let valueArray = [];
        let postsArray =  [];
        if (user !== null) {
            // Checks if the user has no posts or following
            if (user.posts.length === 0 && user.following.length === 0) {
                res.send("No Posts Found")
            } else {
                console.log("User has post new data");
                // Checks each users posts
                user.posts.forEach(function (value) {
                    postsArray.push({postoid: value.postoid, posttime: value.posttime, hearts: value.hearts})
                });
                if (user.following.length !== 0) {
                    console.log("User has following new data");
                    //Search threw each of the following
                    user.following.forEach((followingUser, index, array) => {
                        if (user.following > 0 && index === array.length - 1) {
                            User.findById(followingUser.followingoid, function (err, followingUser) {
                                if (followingUser.posts.length !== 0) {
                                    followingUser.posts.forEach((followingPost, index, array) => {
                                        //Adds each following post to the array
                                        postsArray.push({
                                            postoid: followingPost.postoid,
                                            posttime: followingPost.posttime,
                                            hearts: followingPost.hearts
                                        });
                                        console.log("added data to following new data" + followingPost.postoid);
                                    })
                                } else {
                                    console.log("User had no posts");
                                }
                                //Checks if on the last index
                                if (index === array.length - 1) {
                                    sortPostMoreData(postsArray, res, valueArray, req);
                                    console.log("Sort post data more data");
                                }
                            });
                        } else {
                            console.log("last user in the list new data");
                            // Runs this for the last user in the list
                            User.findById(followingUser.followingoid, function (err, followingUser) {
                                if (followingUser.posts.length !== 0) {
                                    followingUser.posts.forEach((followingPost, index, array) => {
                                        // Add the posts to the array
                                        postsArray.push({
                                            postoid: followingPost.postoid,
                                            posttime: followingPost.posttime,
                                            hearts: followingPost.hearts
                                        });
                                        console.log("added data to following new data" + followingPost.postoid);
                                        // On last iteration move to the next function
                                        if (index === array.length - 1) {
                                            sortPostMoreData(postsArray, res, valueArray, req);
                                            console.log("Sort post data more data");
                                        }
                                    })
                                } else {
                                    // On last iteration move to the next function
                                    sortPostMoreData(postsArray, res, valueArray, req);
                                    console.log("Sort post data more data");
                                }
                            });
                        }
                    })
                } else {
                    // On last iteration move to the next function
                    sortPostMoreData(postsArray, res, valueArray, req);
                    console.log("Sort post data more data");
                }
            }
        }else{
            res.send("No Posts Found")
        }
    });

};

// Sort the posts by date
const sortPostMoreData = function (postsArray,res,valueArray,req) {
    postsArray.sort(function(a, b) {
        a = new Date(a.posttime);
        b = new Date(b.posttime);
        return a>b ? -1 : a<b ? 1 : 0;
    });
    if (postsArray.length !== 0) {
        postData(valueArray, res, postsArray,req)
    }else{
        res.send("no more posts")
    }
};

// Sort the posts by date
const sortPostData = function (postsArray,res,valueArray,req) {
    if (postsArray.length !== 0) {
        postsArray.sort(function (a, b) {
            a = new Date(a.posttime);
            b = new Date(b.posttime);
            return a > b ? -1 : a < b ? 1 : 0;
        });

        postData(valueArray, res, postsArray,req)
    }else{
        res.send("No Posts Found")
    }
};

// Post Data
const postData = function(valueArray, res, postsArray,req){
    // If filter difficulty is not undefined
    if (req.body.difficulty !== undefined){
        console.log("Difficulty Filter applied");
        let difficultyArray = [];
        let filteredArray = [];
        // If a time post time is present then remove all posts after that time
        if (req.body.posttime !== undefined) {
            console.log("Difficulty Filter applied filter by timestamp");
            filterAfterTimeStamp(postsArray, filteredArray, req,res);
        }else{
            filteredArray = filteredArray.concat(postsArray)
        }
        filteredArray.forEach(function (value, postIndex, array) {
            // Find post with id and difficulty
            Post.find({$and: [{"_id": value.postoid },{"difficulty": req.body.difficulty}]})
                .exec(function (err, post) {
                    if (err) {
                        return console.log("Error");
                    }
                    console.log("Difficulty Filter applied got posts with difficulty");
                    // add post to array
                    if (post.length !== 0) {
                        difficultyArray.push(post[0]);
                    }
                    if (postIndex === array.length - 1) {
                        // Limit the amount of posts
                        console.log("Limit the amount of posts");
                        let limitposts = difficultyArray.slice(0, 4);
                        if (limitposts.length !== 0) {
                            // Retrieve images for post
                            limitposts.forEach(function (value, index, array) {
                                get_images(value, valueArray, res,index, array);
                            });
                        } else {
                            res.send("No Posts Found")
                        }
                    }
                });
        });
    //    Filter by ingredients
    }else if(req.body.ingredients !== undefined){
        console.log("Ingredients Filter applied");
        let foundPosts = [];
        let filteredArray = [];
        // Checks if there is a post time if so then remove all before that point
        if (req.body.posttime !== undefined) {
            console.log("Ingredients Filter applied filter by timestamp");
            filterAfterTimeStamp(postsArray, filteredArray, req,res);
        }else{
            filteredArray = filteredArray.concat(postsArray)
        }
        //Search threw each post
        filteredArray.forEach(function (value, postIndex, postArray) {
            Post.findById(value.postoid, function (err, post) {
                let addedOids = false;
                req.body.ingredients.forEach(function (ingredients, index, array) {
                    post.ingredients.forEach(function (postOid, ingredientIndex, ingredientArray) {
                        if (ingredients.ingredientoid === postOid.ingredientoid && !addedOids){
                            foundPosts.push(post);
                            addedOids = true;
                            console.log("Added post to found posts");
                        }
                        if (postIndex === postArray.length -1 && ingredientIndex === ingredientArray.length -1){
                            //Limit the amount of posts to 4
                            console.log("Ingredients Filter applied limit posts");
                            let limitposts = foundPosts.slice(0, 4);
                            limitposts.forEach(function (value, index, array) {
                                try {
                                    Post.findById(value.id, function (err, post) {
                                        get_images(post, valueArray, res, index, array);
                                    });
                                } catch (e) {
                                    console.log(e)
                                }
                            });
                        } else if(index === array.length -1 && postIndex === postArray-1 && foundPosts.length === 0){
                            res.send("no posts found")
                        }
                    });
                });
            });
        });
    //    If filter is top then it will sort by hearts
    }else if(req.body.top !== undefined){
        console.log("Top Filter applied");
        let filteredArray = [];
        // Check if post time is present if so remove elements after timestamp
        if (req.body.posttime !== undefined) {
            console.log("Top Filter applied filter by timestamp");
            filterAfterTimeStamp(postsArray, filteredArray, req,res);
        }else{
            filteredArray = filteredArray.concat(postsArray)
        }
        console.log("Filtered by hearts");
        //Filter to hearts
        filteredArray.sort(function (a, b) {
            a = a.hearts;
            b = b.hearts;
            return a+b
        });
        //Get images of posts
        let limitposts = filteredArray.slice(0, 4);
        console.log(limitposts);
        console.log(req.body.posttime);
        limitposts.forEach(function (value, index, array) {
            try {
                Post.findById(value.postoid, function (err, post) {
                    get_images(post, valueArray, res, index, array);
                });
            } catch (e) {
                console.log(e)
            }
        });
    } else {
        console.log("No Filter just loading data");
        let filteredArray = [];
        // Check if post time is present if so remove elements after timestamp
        if (req.body.posttime !== undefined) {
            console.log("Get more data after timestamp");
            filterAfterTimeStamp(postsArray, filteredArray, req, res);
        }else{
            filteredArray = filteredArray.concat(postsArray)
        }
        //Limit the amount of posts that are there
        let limitposts = filteredArray.slice(0, 4);
        limitposts.forEach(function (value, index, array) {
            try {
                Post.findById(value.postoid, function (err, post) {
                    get_images(post, valueArray, res, index, array);
                });
            } catch (e) {
                console.log(e)
            }
        });
    }
};

// Filter out times before the timestamp
const filterAfterTimeStamp = function(postsArray,filteredArray,req,res){
    try {
        console.log("Posts Array" + postsArray);
        postsArray.forEach(function (value, index, array) {
            unixtimeStampPostTime = Math.floor(value.posttime / 1000);
            unitxTimeStampLasttime = Math.floor(Date.parse(req.body.posttime) / 1000);
            if (unixtimeStampPostTime < unitxTimeStampLasttime) {
                if (filteredArray.length <= 4) {
                    console.log("Filtered Array" + filteredArray);
                    filteredArray.push(value)
                }
            }
            if (index === array.length - 1 && filteredArray.length === 0) {
                console.log("no post")
            }
        });
    }catch (e) {
        console.log(e)
    }
};

exports.put_heart = function (req,res) {
    Post.findByIdAndUpdate(req.params.id, {$inc: {hearts: 1}, $push: { userhearts: {userId: req.body.userId} }}, {new: true}, function (err, updatePost) {
        if (err) {
            res.send(err);
        }
        res.send("Liked Post")
    });
};

exports.update_heart_user = function(req,res){
    User.findById(req.body.userId, function (err, user) {
        if (err) return console.log(err);

        //Search threw posts to add useroid
        user.posts.forEach((post, index, array) => {
            if (post.postoid === req.params.id){
                post.hearts++;
            }
            if(index === array.length - 1) {
                user.save(function (err) {
                    if (err) {
                        return console.log(err);
                    }
                    console.log("User hearted");

                    res.send("Liked Post")
                });
            }
        });
    });
};

exports.remove_heart = function (req,res) {
    Post.findByIdAndUpdate(req.params.id, {$inc: {hearts: -1}, $pull: { userhearts: {userId: req.body.userId} }}, {new: true}, function (err, post) {
        if (err) {
            res.send(err);
        } else {
            console.log("Remove Heart");
            res.send(post);
        }
    });
};

exports.add_comment = function(req,res){
    Post.findById(req.params.id, function (err, post) {
        if (err) return console.log(err);

        post.comments.push({commentString: req.body.commentString, userOid: req.body.userOid});

        post.save(function (err) {
            if (err) {
                return console.log(err);
            }

            res.send('Added comment to the post')
        });
    });
};


exports.add_ingredients = function(req,res){
    Post.findById(req.params.id, function (err, post) {
        if (err) return res.send(err);
            // Add ingredients to basket
            req.body.basket.forEach(function(postBasket, basketIndex, basketArray) {
                post.ingredients.push({ingredientoid: postBasket.ingredientoid});
                if (basketIndex === basketArray.length - 1) {
                    post.save(function (err) {
                        if (err) {
                            return res.send(err);
                        }

                        res.send('Added ingredient to post')
                    });
                }
            });
    });
};

let postToFolder = "";
const multer = require('multer');
//Image upload location
const uploadLocation = multer({
    dest: "/home/ubuntu/cookingappapi/controllers"
});

// Limit the amount of image files can be added
const multerController = uploadLocation.array('imageFiles',5);
const handleError = (err, res) => {
    res
        .status(500)
        .contentType("text/plain")
        .end("Oops! Something went wrong!");
};

//Uploading images
module.exports.uploadFiles = async (req, res) => {
    try {
        postToFolder = req.params.id;
        let dir = __dirname + "/uploads/post" + postToFolder;
        let postValue;

        Post.findById(postToFolder, function (err, post) {
            if (err){
                return console.log(err);
            }

            post.imagePath = dir;
            postValue = post;
            post.save(function (err) {
                if (err) {
                    return console.log(err);
                }
            });
        });

        multerController(req, res, err => {
            req.files.forEach(function(value, index, array){
                postingMultipleImages(value,res,dir);

                if (index === array.length - 1) {
                    res.send("All images posted")
                }
            });
        })
    } catch (error) {
        if (error.code === "LIMIT_UNEXPECTED_FILE") {
            return res.send("Too many files to upload.");
        }
        return res.send(`Error when trying upload many files: ${error}`);
    }
};

//Add multiple images
function postingMultipleImages(value,res,dir){
    const fileName = value.originalname;

    const tempPath = value.path;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    const targetPath = path.join(dir, fileName);

    fs.rename(tempPath, targetPath, err => {
        if (err) return handleError(err, res);
    });
}

//Get images
const get_images = function(post,valueArray,res,index,array) {
    try {
        let imageArray = [];
        if (post !== null) {
            //Read the Directory
            console.log("Reading directory");
            fs.readdir(post.imagePath, (err, files) => {
                files.forEach(file => {
                    let absolutePath = path.join(post.imagePath, file);

                    // Read the File
                    fs.readFile(absolutePath, function read(err, data) {
                        if (err) {
                            throw err;
                        }
                        console.log("Reading image");
                        // Add image file to array
                        imageArray.push(Buffer.from(data).toString('base64'));
                        if(files.length === imageArray.length) {
                            valueArray.push({data: imageArray, post: post});
                            if (valueArray.length === array.length) {
                                res.send({'postArray': valueArray});
                                console.log("Post Array");
                            }
                        }
                    });
                });
            });
        }

    }catch (e) {
        console.log(e)
    }
};


