const express = require('express');
const router = express.Router();

const post_controller = require('../controllers/post.controller');

router.post('/create/:id', post_controller.post_create);
router.post('/upload/:id',post_controller.uploadFiles);
router.post('/comment/:id',post_controller.add_comment);
router.post('/ingredients/:id',post_controller.add_ingredients);
router.post('/id', post_controller.post_details);
router.post('/newdata', post_controller.post_new_data);
router.post('/heart/user/:id',post_controller.update_heart_user);



router.put('/heart/:id', post_controller.put_heart);
router.put('/removeheart/:id', post_controller.remove_heart);

module.exports = router;
