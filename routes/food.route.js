const express = require('express');
const router = express.Router();

const food_controller = require('../controllers/food.controller');

router.post('/create', food_controller.add_food);
router.post('/upload/:id',food_controller.uploadFiles);
router.post('/foodLoad',food_controller.find_food_name_initial);
router.post('/byIdLoad',food_controller.cupboard_id_initial);
router.post('/byIdLoadBasket',food_controller.basket_id_initial);
router.post('/shop', food_controller.find_food_shop);
router.post('/name', food_controller.find_food_name);
router.post('/partialTextSearch', food_controller.find_food_partialtext);
router.post('/findId',food_controller.find_food_postid);


router.get('/foodId/:id', food_controller.find_food_id);


router.put('/shopReliability/:id',food_controller.put_shop_reliability);
router.put('/priceReliability/:id',food_controller.put_price_reliability);
router.put('/expirationTimeReliability/:id',food_controller.put_expirationTime_reliability);
router.put('/imagePathReliability/:id',food_controller.put_imagePath_reliability);

router.put('/removeExpirationTime/:id',food_controller.remove_expirationTime_reliability);
router.put('/removeImagePath/:id',food_controller.remove_imagePath_reliability);
router.put('/removePrice/:id',food_controller.remove_price_reliability);
router.put('/removeShop/:id',food_controller.remove_shop_reliability);



module.exports = router;
