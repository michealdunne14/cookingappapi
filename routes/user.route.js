const express = require('express');
const router = express.Router();

const user_controller = require('../controllers/user.controller');

// User Commands
router.post('/create', user_controller.user_create);
router.post('/upload/:id',user_controller.uploadFiles);
router.post('/following',user_controller.following_data);
router.post('/email',user_controller.user_details_email);
router.post('/cupboard/:id',user_controller.add_food_to_cupboard);
router.post('/username',user_controller.user_details_username);
router.post('/basket/:id',user_controller.add_food_to_basket);
router.post('/deleteItemFromBasket', user_controller.remove_from_basket);
router.post('/deleteItemFromCupboard', user_controller.remove_from_cupboard);
router.post('/changeCounterBasket',user_controller.change_counter_basket);
router.post('/changeCounterCupboard',user_controller.change_counter_cupboard);


router.get('/id/:id', user_controller.user_details);


router.put('/update/:id', user_controller.user_update);
router.put('/follow/:id', user_controller.user_follow);
router.put('/unfollow/:id', user_controller.user_unfollow);
router.put('/saved/:id',user_controller.add_saved_post);

router.delete('/delete/:id', user_controller.user_delete);
module.exports = router;
