const util = require("util");
const fs = require('fs');
const AWS = require('aws-sdk');
var config = require('./config');

const s3 = new AWS.S3({
    accessKeyId: config.ID,
    secretAccessKey: config.SECRET
});

const params = {
    Bucket: config.BUCKET_NAME,
    CreateBucketConfiguration: {
        // Set your region here
        LocationConstraint: "eu-west-1"
    }
};

// var params = { Bucket: config.bucket, Key: req.params.imageId };


exports.uploadFile = (targetPath,postToFolder,fileName) => {
    // Read content from the file
    const fileContent = fs.readFileSync(targetPath);

    // Setting up S3 upload parameters
    const params = {
        Bucket: config.BUCKET_NAME,
        Key: 'folder'+ postToFolder + '/' + Date.now() + fileName, // File name you want to save as in S3
        Body: fileContent
    };

    // Uploading files to the bucket
    s3.upload(params, function(err, data) {
        if (err) {
            throw err;
        }
        console.log(`File uploaded successfully. ${data.Location}`);
    });
};

exports.listBuckets = async function (id,res) {
    const response = await s3.listObjectsV2({
        Bucket: config.BUCKET_NAME,
        Prefix: 'folder' + id
    }).promise();
    res.send(response);
};



