const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ShopSchema = new Schema({
    shopName: {type: String, required: true, unique: true},
    shopReliability: {type: String, required: true, unique: true},
});


// Export the model
module.exports = mongoose.model('Shop', ShopSchema);
