const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let FoodSchema = new Schema({
    name: {type: String, required: true, unique: true},
    price: {type: Number, required: true},
    priceReliability: {type: Number, default: 0},
    shop: {type: String, required: true},
    shopReliability: {type: Number, default: 0},
    expirationTime: {type: Number, default: 0},
    expirationTimeReliability: {type: Number, default: 0},
    imagePath: {type: String, default: ''},
    imagePathReliability: {type: Number, default: 0},
});


// Export the model
module.exports = mongoose.model('Food', FoodSchema);
