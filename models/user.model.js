const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UsersSchema = new Schema({
    name: {type: String, required: true},
    email: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    username: {type: String, required: true, unique: true},
    signupdate: {type: String, required: true},
    active: {type: Boolean,default: false},
    profilePicture: {type: String, default: ''},
    passwordResetToken: String,
    passwordResetExpires: Date,
    verified: {type: Boolean, default: false},
    followers: [{ _id: false, followeroid: String }],
    following: [{ _id: false, followingoid: String }],
    cupboard: [{ _id: false, cupboardoid: String, foodPurchasedCounter: Number}],
    basket: [{ _id: false, basketoid: String, counter: Number }],
    saved: [{ _id: false, savedoid: String }],
    posts: [{ _id: false, postoid: String,posttime: Date,hearts: Number }]
});


// Export the model
module.exports = mongoose.model('User', UsersSchema);
