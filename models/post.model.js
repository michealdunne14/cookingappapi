const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let PostSchema = new Schema({
    title: {type: String, required: true},
    description: {type: String, required: true},
    imagePath: {type: String, default: ''},
    useroid: {type: String, required: true},
    ingredients: [{ _id: false, ingredientoid: String }],
    method: [{ methodStep: String }],
    completionTime: {type: String},
    comments: [{ _id: false, commentString: String, userOid: String }],
    hearts: {type: Number, default: 0},
    difficulty: {type: String, default: '',required: true},
    userhearts: [{  _id: false,userId: String }],
    posttime: Date
});


// Export the model
module.exports = mongoose.model('Post', PostSchema);
